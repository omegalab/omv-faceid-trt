#ifndef OMV_OPTIONS_H
#define OMV_OPTIONS_H

#include <map>
#include <string>
#include <type_traits>
#include <variant>

#include "CLI/CLI.hpp"

#include "omvfid_version.h"
namespace omv {

class Options {
 public:
  using Variant = std::variant<int, float, std::string, bool>;

  static Options& Instance() {
    static Options s;
    return s;
  }

  Options(Options const&) = delete;
  Options& operator=(Options const&) = delete;

  void Init(int argc, char** argv);
  void Exit(CLI::ParseError&);
  void Parse();

  class ProxyValue {
   public:
    ProxyValue(Options* opts, const std::string& k) : opts_{opts}, k_{k} {}

    template <typename T>
    operator T() {
      return opts_->Get<T>(k_);
    }

   private:
    Options* opts_;
    std::string k_;
  };

  ProxyValue operator[](const std::string& key) { return ProxyValue(this, key); }

  template <typename T>
  inline T Get(const std::string& key) const {
    return std::get<T>(data_.at(key));
  }

 private:
  int argc_;
  char** argv_;

  Options();
  CLI::App opts_;
  std::map<std::string, Variant> data_{{"data", "/usr/local/share/omvfid/models"},
                                       {"dtype", 0},
                                       {"rebuild", false},
                                       {"batch-size", 1},
                                       {"min-face-size", 60},
                                       {"min-det-size", 12},
                                       {"max-scene-faces", 10}};
};

}  // namespace omv

#endif  // OMV_OPTIONS_H
