#include "pnet.h"

#include <cstdio>

#include "options.h"

namespace omv {
size_t PNet::pnet_index_ = 0;

PNet::PNet() {
  loc_ = std::make_shared<PBox>();
  score_ = std::make_shared<PBox>();
  rgb_ = std::make_shared<PBox>();
}

PNet::~PNet() {
  nvcaffeparser1::shutdownProtobufLibrary();

  if (loc_->pdata)
    std::free(loc_->pdata);
  if (score_->pdata)
    std::free(score_->pdata);
  if (rgb_->pdata)
    std::free(rgb_->pdata);

  CudaMemFree();
}

void PNet::Init(size_t rows, size_t cols) {
  input_w_ = cols;
  input_h_ = rows;

  SetInputBlobName(kInputBlobName);
  SetOutputLocName(kOutputLocName);
  SetOutputProbName(kOutputProbName);

  auto& opts = Options::Instance();
  const std::string& data_path = opts["data"];
  const bool rebuild_engine = opts["rebuild"];

  const std::string& engine_path = fmt::format("{}/{}.{}.{}", data_path, "det1_relu", ++pnet_index_, "engine");
  //  const std::string& engine_path = fmt::format("{}/{}{}.{}", data_path, "det1_relu", ++pnet_index_, "engine");

  SetEnginePath(engine_path);

  if (Helper::FileExists(engine_path) && !rebuild_engine)
    LoadEngine();
  else
    BuildCaffeEngine();

  auto engine = GetEngine();

  if (nullptr == engine)
    throw std::runtime_error(fmt::format("null pnet engine: {}", GetEnginePath()));

  input_index_ = engine->getBindingIndex(kInputBlobName.c_str());
  output_prob_ = engine->getBindingIndex(kOutputProbName.c_str());
  output_loc_ = engine->getBindingIndex(kOutputLocName.c_str());

  score_->width = static_cast<size_t>(ceil((input_w_ - 2) / 2.) - 4);
  score_->height = static_cast<size_t>(ceil((input_h_ - 2) / 2.) - 4);
  score_->channel = 2;

  loc_->width = static_cast<size_t>(ceil((input_w_ - 2) / 2.) - 4);
  loc_->height = static_cast<size_t>(ceil((input_h_ - 2) / 2.) - 4);
  loc_->channel = 4;

  out_prob_size_ = score_->width * score_->height * score_->channel;
  out_loc_size_ = loc_->width * loc_->height * loc_->channel;

  rgb_->pdata = static_cast<float*>(std::malloc(kInputC * input_h_ * input_w_ * sizeof(float)));
  score_->pdata = static_cast<float*>(std::malloc(out_prob_size_ * sizeof(float)));
  loc_->pdata = static_cast<float*>(std::malloc(out_loc_size_ * sizeof(float)));

  CudaMemAlloc();
}

void PNet::Run(cv::Mat& image, float scale) {
  Helper::Mat2Array(image, rgb_);
  //  auto engine = GetEngine();
  //  cudaStream_t stream;
  //  cudaStreamCreate(&stream);
  //  auto ctx = engine->createExecutionContext();
  CudaMemCpyAsyncH2D(buffers_[input_index_], rgb_->pdata, kInputC * input_h_ * input_w_ * sizeof(float));
  //  cudaMemcpyAsync(buffers_[input_index_], rgb_->pdata, kInputC * input_h_ * input_w_ * sizeof(float),
  //                  cudaMemcpyHostToDevice, stream);
  Enqueue(buffers_);
  //  ctx->enqueue(1, buffers_, stream, nullptr);
  //  ctx->enqueueV2(buffers_, stream, nullptr);
  //  cudaMemcpyAsync(score_->pdata, buffers_[output_prob_], out_prob_size_ * sizeof(float),
  //  cudaMemcpyDeviceToHost,
  //                  stream);
  //  cudaMemcpyAsync(loc_->pdata, buffers_[output_loc_], out_loc_size_ * sizeof(float), cudaMemcpyDeviceToHost,
  //                  stream);
  CudaMemCpyAsyncD2H(score_->pdata, buffers_[output_prob_], out_prob_size_ * sizeof(float));
  CudaMemCpyAsyncD2H(loc_->pdata, buffers_[output_loc_], out_loc_size_ * sizeof(float));
  //  cudaStreamSynchronize(stream);
  //  cudaStreamDestroy(stream);
  CudaStreamSynchronize();
  GenerateBBox(scale);
}

void PNet::Clear() {
  bboxes_.clear();
  bbox_scores_.clear();
}

std::vector<BBox::Sptr>& PNet::GetBBoxes() { return bboxes_; }
std::vector<OrderScore::Sptr>& PNet::GetBBoxScores() { return bbox_scores_; }

float PNet::GetNMSThreshold() const { return nms_threshold_; }

void PNet::CudaMemAlloc() {
  auto batch_size = GetBatchSize();

  CHECK(cudaMalloc(&buffers_[input_index_], batch_size * kInputC * input_h_ * input_w_ * sizeof(float)));
  CHECK(cudaMalloc(&buffers_[output_prob_], batch_size * out_prob_size_ * sizeof(float)));
  CHECK(cudaMalloc(&buffers_[output_loc_], batch_size * out_loc_size_ * sizeof(float)));
  CreateCudaStream();
}

void PNet::CudaMemFree() {
  DestroyCudaStream();
  CHECK(cudaFree(buffers_[input_index_]));
  CHECK(cudaFree(buffers_[output_prob_]));
  CHECK(cudaFree(buffers_[output_loc_]));
}

void PNet::BuildCaffeEngine() {
  auto& opts = Options::Instance();
  const std::string& data_path = opts["data"];

  const std::string& proto_path = fmt::format("{}/{}", data_path, "det1_relu.prototxt");
  const std::string& model_path = fmt::format("{}/{}", data_path, "det1_relu.caffemodel");
  const std::string& temp_proto_path = fmt::format("{}/{}.{}.{}", data_path, "det1_relu", pnet_index_, "prototxt");

  SetModelPath(model_path);

  size_t first_space = 16;
  size_t second_space = 4;
  std::fstream proto_file;
  proto_file.open(proto_path, std::ios::in);
  std::stringstream buffer;
  buffer << proto_file.rdbuf();
  std::string contents(buffer.str());
  std::string::size_type position_h, position_w;
  position_h = contents.find("dim");

  while (std::isdigit(contents[position_h + first_space]))
    contents.erase(position_h + first_space, 1);

  contents.insert(position_h + first_space, std::to_string(input_h_));
  position_w = contents.find("dim", position_h + first_space);

  while (std::isdigit(contents[position_w + second_space]))
    contents.erase(position_w + second_space, 1);

  contents.insert(position_w + second_space, std::to_string(input_w_));

  proto_file.close();
  proto_file.open(temp_proto_path, std::ios::out);
  proto_file.write(contents.c_str(), static_cast<std::streamsize>(contents.size()));
  proto_file.close();

  SetProtoPath(temp_proto_path);
  BuildEngine<nvcaffeparser1::ICaffeParser>(fmt::format("pnet.{}", pnet_index_));

  if (Helper::FileExists(temp_proto_path))
    std::remove(temp_proto_path.c_str());
}

void PNet::GenerateBBox(float scale) {
  size_t stride = 2;
  size_t cellsize = 12;
  int count = 0;

  float* p = score_->pdata + score_->width * score_->height;
  float* plocal = loc_->pdata;

  auto bbox = std::make_shared<BBox>();
  auto order = std::make_shared<OrderScore>();

  for (size_t row = 0; row < score_->height; ++row) {
    for (size_t col = 0; col < score_->width; ++col) {
      if (*p > p_threshold_) {
        bbox->score = *p;
        order->score = *p;
        order->ori_order = count;
        bbox->x1 = static_cast<size_t>(round((stride * row + 1) / scale));
        bbox->y1 = static_cast<size_t>(round((stride * col + 1) / scale));
        bbox->x2 = static_cast<size_t>(round((stride * row + 1 + cellsize) / scale));
        bbox->y2 = static_cast<size_t>(round((stride * col + 1 + cellsize) / scale));
        bbox->exist = true;
        bbox->area = (bbox->x2 - bbox->x1) * (bbox->y2 - bbox->y1);
        for (size_t channel = 0; channel < 4; ++channel)
          bbox->regre_coord[channel] = *(plocal + channel * loc_->width * loc_->height);
        bboxes_.push_back(bbox);
        bbox_scores_.push_back(order);
        ++count;
      }
      ++p;
      ++plocal;
    }
  }
}

}  // namespace omv
