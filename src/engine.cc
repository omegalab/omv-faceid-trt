#include "engine.h"

#include <cassert>

#include "logger.h"
#include "options.h"

namespace omv {

IEngine::IEngine() {
  Options& opts = Options::Instance();
  int datatype = opts["dtype"];

  batch_size_ = opts["batch-size"];
  data_type_ = static_cast<nvinfer1::DataType>(datatype);
}

void IEngine::LoadEngine() {
  if (engine_path_.empty())
    throw std::runtime_error("engine path empty");

  std::vector<char> trt_model_stream;
  std::streamsize size{0};
  std::ifstream engine_file(engine_path_, std::ios::binary);

  engine_file.seekg(0, engine_file.end);
  size = engine_file.tellg();
  engine_file.seekg(0, engine_file.beg);
  trt_model_stream.resize(static_cast<size_t>(size));
  engine_file.read(trt_model_stream.data(), size);
  engine_file.close();

  auto runtime = MakeUniqueInfer<nvinfer1::IRuntime>(nvinfer1::createInferRuntime(Logger::Instance()));

  if (nullptr == runtime)
    throw std::runtime_error(fmt::format("failed to create runtime: {}", engine_path_));

  engine_ = MakeUniqueInfer<nvinfer1::ICudaEngine>(
      runtime->deserializeCudaEngine(trt_model_stream.data(), static_cast<size_t>(size), nullptr), InferDeleter());

  if (nullptr == engine_)
    throw std::runtime_error(fmt::format("failed to load engine: {}", engine_path_));

  CreateContext();

  LOG(INFO) << "loaded engine: " << engine_->getName() << " (size: " << Helper::HumanSize(size) << ')';
}

void IEngine::CreateContext() {
  ctx_ = MakeSharedInfer<nvinfer1::IExecutionContext>(engine_->createExecutionContext());
}

void IEngine::StoreEngine() {
  if (engine_path_.empty())
    throw std::runtime_error("engine path empty");

  std::ofstream fs;
  fs.open(engine_path_);
  auto serialized_engine = MakeUniqueInfer<nvinfer1::IHostMemory>(engine_->serialize());
  fs.write(static_cast<char*>(serialized_engine->data()), static_cast<std::streamsize>(serialized_engine->size()));
  fs.close();
}

void IEngine::ParseUff(NetworkSptr network) {
  auto parser = MakeUniqueInfer<nvuffparser::IUffParser>(nvuffparser::createUffParser());
  parser->registerInput("input", nvinfer1::Dims3(160, 160, 3), nvuffparser::UffInputOrder::kNHWC);
  parser->registerOutput("embeddings");

  if (!parser->parse(model_path_.c_str(), *network, data_type_))
    throw std::runtime_error(fmt::format("failed to parse UFF file: {}", model_path_));
}

void IEngine::ParseCaffe(NetworkSptr network) {
  auto parser = MakeUniqueInfer<nvcaffeparser1::ICaffeParser>(nvcaffeparser1::createCaffeParser());
  auto blob_name_to_tensor = parser->parse(proto_path_.c_str(), model_path_.c_str(), *network, data_type_);

  if (!blob_name_to_tensor)
    throw std::runtime_error(fmt::format("failed to parse Caffe file: {}", model_path_));

  MakeOutputs();

  for (const auto& s : outputs_)
    network->markOutput(*blob_name_to_tensor->find(s.c_str()));
}

IEngine::EngineSptr IEngine::GetEngine() const { return engine_; }
IEngine::ContextSptr IEngine::GetContext() const { return ctx_; }

void IEngine::SetModelPath(const std::string& path) { model_path_ = path; }
void IEngine::SetEnginePath(const std::string& path) { engine_path_ = path; }
void IEngine::SetProtoPath(const std::string& path) { proto_path_ = path; }
void IEngine::SetInputBlobName(const std::string& name) { input_blob_name_ = name; }
void IEngine::SetOutputProbName(const std::string& name) { output_prob_name_ = name; }
void IEngine::SetOutputLocName(const std::string& name) { output_loc_name_ = name; }
void IEngine::SetOutputPointName(const std::string& name) { output_point_name_ = name; }

std::string IEngine::GetModelPath() const { return model_path_; }
std::string IEngine::GetEnginePath() const { return engine_path_; }
std::string IEngine::GetProtoPath() const { return proto_path_; }
std::string IEngine::GetInputBlobName() const { return input_blob_name_; }
std::string IEngine::GetOutputProbName() const { return output_prob_name_; }
std::string IEngine::GetOutputLocName() const { return output_loc_name_; }
std::string IEngine::GetOutputPointName() const { return output_point_name_; }

nvinfer1::DataType IEngine::GetDataType() const { return data_type_; }
size_t IEngine::GetBatchSize() const { return static_cast<size_t>(batch_size_); }

void IEngine::MakeOutputs() {
  if (!output_prob_name_.empty())
    outputs_.push_back(output_prob_name_);
  if (!output_loc_name_.empty())
    outputs_.push_back(output_loc_name_);
  if (!output_point_name_.empty())
    outputs_.push_back(output_point_name_);
}
void IEngine::CreateCudaStream() { CHECK(cudaStreamCreate(&stream_)); }
void IEngine::DestroyCudaStream() { CHECK(cudaStreamDestroy(stream_)); }
void IEngine::CudaMemCpyAsyncH2D(void* dst, const void* src, size_t size) {
  CHECK(cudaMemcpyAsync(dst, src, static_cast<size_t>(batch_size_) * size, cudaMemcpyHostToDevice, stream_));
}

void IEngine::CudaMemCpyAsyncD2H(void* dst, const void* src, size_t size) {
  CHECK(cudaMemcpyAsync(dst, src, static_cast<size_t>(batch_size_) * size, cudaMemcpyDeviceToHost, stream_));
}

void IEngine::Enqueue(void** bindings) {
  ctx_->enqueue(static_cast<int>(batch_size_), bindings, stream_, nullptr);
}

void IEngine::CudaStreamSynchronize() { cudaStreamSynchronize(stream_); }

}  // namespace omv
