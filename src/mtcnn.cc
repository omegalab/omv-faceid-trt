#include "mtcnn.h"

#include "logger.h"
#include "options.h"

namespace omv {

void MTCNN::Init(size_t rows, size_t cols) {
  Options& opts = Options::Instance();

  nms_thresholds_ = {0.7f, 0.7f, 0.7f};
  int min_face_size = opts["min-face-size"];
  int min_det_size = opts["min-det-size"];

  float minl = rows < cols ? rows : cols;

  float m = static_cast<float>(min_det_size) / min_face_size;
  minl *= m;

  float factor = 0.709f;
  int factor_count = 0;

  while (minl > min_det_size) {
    if (factor_count > 0)
      m = m * factor;

    scales_.push_back(m);
    minl *= factor;
    ++factor_count;
  }

  float minside = rows < cols ? rows : cols;
  size_t count = 0;

  for (auto it = scales_.begin(); it != scales_.end(); ++it) {
    if (*it > 1)
      LOG(WARN) << "min-face-size is too small (" << min_face_size << ')';

    if (*it < (min_det_size / minside)) {
      scales_.resize(count);
      break;
    }
    ++count;
  }

  LOG(DEBUG) << "scales: " << Helper::PrintRange(scales_.begin(), scales_.end());

  InitPNets(rows, cols);
}

void MTCNN::InitPNets(size_t rows, size_t cols) {
  for (size_t i = 0; i < scales_.size(); ++i) {
    size_t changed_h = static_cast<size_t>(std::ceil(rows * scales_.at(i)));
    size_t changed_w = static_cast<size_t>(std::ceil(cols * scales_.at(i)));
    pnets_.emplace_back(std::make_shared<PNet>());
    pnets_[i]->Init(changed_h, changed_w);
    LOG(DEBUG) << "changed height: " << changed_h << ", changed width: " << changed_w;
  }

  rnet_ = std::make_shared<RNet>();
  rnet_->Init(24, 24);

  onet_ = std::make_shared<ONet>();
  onet_->Init(48, 48);
}

void MTCNN::FindFaces(cv::Mat& img, std::vector<BBox::Sptr>& dst) {
  first_bboxes_.clear();
  second_bboxes_.clear();
  third_bboxes_.clear();
  first_order_scores_.clear();
  second_order_scores_.clear();
  third_order_scores_.clear();

  int count{0};

  for (size_t i = 0; i < scales_.size(); ++i) {
    int changed_h = static_cast<int>(std::ceil(img.rows * scales_.at(i)));
    int changed_w = static_cast<int>(std::ceil(img.cols * scales_.at(i)));

    cv::resize(img, resized_image_, cv::Size(changed_w, changed_h), 0, 0, cv::INTER_LINEAR);
    pnets_[i]->Run(resized_image_, scales_[i]);

    auto& pnet_bboxes = pnets_[i]->GetBBoxes();
    auto& pnet_bbox_scores = pnets_[i]->GetBBoxScores();
    float pnet_nms_threshold = pnets_[i]->GetNMSThreshold();

    LOG(INFO) << "pnets_" << i << " size " << pnet_bboxes.size();

    Helper::NMS(pnet_bboxes, pnet_bbox_scores, pnet_nms_threshold);

    for (auto it = pnet_bboxes.begin(); it != pnet_bboxes.end(); ++it) {
      if ((*it)->exist) {
        first_bboxes_.push_back(*it);
        auto score = std::make_shared<OrderScore>((*it)->score, count);
        first_order_scores_.push_back(score);
        ++count;
      }
    }

    pnets_[i]->Clear();
  }

  LOG(INFO) << "faces found: " << first_bboxes_.size();

  if (count == 0)
    return;
}

}  // namespace omv
