#ifndef OMV_OMVFID_H
#define OMV_OMVFID_H

#include <atomic>
#include <csignal>

#include "facenet.h"
#include "mtcnn.h"
#include "options.h"

namespace omv {

class SignalHandler {
 public:
  SignalHandler() = default;
  ~SignalHandler() = default;

  void RegisterCallback(std::function<void()>&& cb);
  static void CatchSignal(int s);

 private:
  static std::function<void()> callback_;
};

class OmvFID {
 public:
  OmvFID(int argc, char* argv[]);

  void Init();
  void Start();
  void Stop();

  void ShowHelpAndExit(CLI::ParseError&);
  void ShowVersionAndExit(omv::ShowVersion&);

 private:
  SignalHandler signal_handler_;

  FaceNet::Uptr facenet_{nullptr};
  MTCNN::Uptr mtcnn_{nullptr};

  std::atomic_bool stopped_{false};
};

}  // namespace omv

#endif  // OMV_OMVFID_H
