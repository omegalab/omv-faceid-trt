#ifndef OMV_FACENET_H
#define OMV_FACENET_H

#include <NvInfer.h>
#include <NvUffParser.h>

#include <memory>

#include "engine.h"
#include "helper.h"

namespace omv {

class FaceNet : public IEngine {
 public:
  using Uptr = std::unique_ptr<FaceNet>;
  using Sptr = std::shared_ptr<FaceNet>;
  FaceNet() = default;
  ~FaceNet() override;

  void Init(size_t, size_t) override;
  void Run(cv::Mat& image, float scale = 0) override {}

 protected:
  void CudaMemAlloc() override;
  void CudaMemFree() override;

 private:
  int input_index_;
  int output_index_;

  void* buffers_[2];
};

}  // namespace omv

#endif  // OMV_FACENET_H
