#ifndef OMV_IENGINE_H
#define OMV_IENGINE_H

#include <NvCaffeParser.h>
#include <NvInfer.h>
#include <NvUffParser.h>
#include <cuda_runtime_api.h>

#include <cstdlib>
#include <type_traits>

#include <opencv2/opencv.hpp>

#include "fmt/format.h"

#include "helper.h"
#include "logger.h"

namespace omv {

class IEngine {
 public:
  using NetworkSptr = InferSptr<nvinfer1::INetworkDefinition>;
  using EngineSptr = InferSptr<nvinfer1::ICudaEngine>;
  using ContextSptr = InferSptr<nvinfer1::IExecutionContext>;

  IEngine();
  virtual ~IEngine() = default;

  virtual void Init(size_t rows, size_t cols) = 0;
  virtual void Run(cv::Mat& image, float scale = 0) = 0;

  void LoadEngine();
  void StoreEngine();

  template <typename P>
  void BuildEngine(const std::string& engine_name) {
    auto builder = MakeUniqueInfer<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(Logger::Instance()));
    auto config = MakeUniqueInfer<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());
    auto network = MakeSharedInfer<nvinfer1::INetworkDefinition>(builder->createNetworkV2(0));
    //    auto network = MakeSharedInfer<nvinfer1::INetworkDefinition>(builder->createNetwork());
    network->setName(engine_name.c_str());

    config->setFlag(nvinfer1::BuilderFlag::kGPU_FALLBACK);
    config->setFlag(nvinfer1::BuilderFlag::kSTRICT_TYPES);

    if (data_type_ == nvinfer1::DataType::kHALF)
      config->setFlag(nvinfer1::BuilderFlag::kFP16);
    else if (data_type_ == nvinfer1::DataType::kINT8)
      config->setFlag(nvinfer1::BuilderFlag::kINT8);

    builder->setMaxBatchSize(batch_size_);

    if constexpr (std::is_same_v<P, nvuffparser::IUffParser>) {
      ParseUff(network);
      //      config->setMaxWorkspaceSize(1_GiB);
    } else if constexpr (std::is_same_v<P, nvcaffeparser1::ICaffeParser>) {
      ParseCaffe(network);
      config->setMaxWorkspaceSize(1 << 25);
    } else
      throw std::invalid_argument("unsupported parser");

    engine_ = MakeSharedInfer<nvinfer1::ICudaEngine>(builder->buildEngineWithConfig(*network, *config));
    //    engine_ = MakeSharedInfer<nvinfer1::ICudaEngine>(builder->buildCudaEngine(*network));

    if (nullptr == engine_)
      throw std::runtime_error(fmt::format("failed to build engine: {}", engine_name));

    StoreEngine();
    CreateContext();

    LOG(INFO) << "build engine: " << engine_->getName();
  }

 protected:
  void CreateContext();
  size_t GetBatchSize() const;
  EngineSptr GetEngine() const;
  ContextSptr GetContext() const;
  nvinfer1::DataType GetDataType() const;
  cudaStream_t& GetStream() { return stream_; }

  void SetModelPath(const std::string& path);
  void SetEnginePath(const std::string& path);
  void SetProtoPath(const std::string& path);
  void SetInputBlobName(const std::string& name);
  void SetOutputProbName(const std::string& name);
  void SetOutputLocName(const std::string& name);
  void SetOutputPointName(const std::string& name);

  std::string GetModelPath() const;
  std::string GetEnginePath() const;
  std::string GetProtoPath() const;
  std::string GetInputBlobName() const;
  std::string GetOutputProbName() const;
  std::string GetOutputLocName() const;
  std::string GetOutputPointName() const;

  virtual void CudaMemAlloc() = 0;
  virtual void CudaMemFree() = 0;

  void CreateCudaStream();
  void DestroyCudaStream();
  void CudaMemCpyAsyncH2D(void* dst, const void* src, size_t size);
  void CudaMemCpyAsyncD2H(void* dst, const void* src, size_t size);

  void Enqueue(void** bindings);
  void CudaStreamSynchronize();

 private:
  EngineSptr engine_{nullptr};
  ContextSptr ctx_{nullptr};
  cudaStream_t stream_;

  nvinfer1::DataType data_type_;
  int batch_size_{1};

  std::string model_path_;
  std::string engine_path_;
  std::string proto_path_;
  std::string input_blob_name_;
  std::string output_prob_name_;
  std::string output_loc_name_;
  std::string output_point_name_;
  std::vector<std::string> outputs_;

  void MakeOutputs();
  void ParseUff(NetworkSptr);
  void ParseCaffe(NetworkSptr);
};

}  // namespace omv

#endif  // OMV_IENGINE_H
