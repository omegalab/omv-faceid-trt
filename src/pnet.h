#ifndef OMV_PNET_H
#define OMV_PNET_H

#include "engine.h"
#include "helper.h"

namespace omv {

class PNet : public IEngine {
 public:
  using Sptr = std::shared_ptr<PNet>;
  using Uptr = std::unique_ptr<PNet>;

  PNet();
  ~PNet() override;

  void Init(size_t rows, size_t cols) override;
  void Run(cv::Mat& image, float scale) override;
  void Clear();

  std::vector<BBox::Sptr>& GetBBoxes();
  std::vector<OrderScore::Sptr>& GetBBoxScores();
  float GetNMSThreshold() const;

 protected:
  void CudaMemAlloc() override;
  void CudaMemFree() override;

 private:
  static size_t pnet_index_;

  const std::string kInputBlobName{"data"};
  const std::string kOutputLocName{"conv4-2"};
  const std::string kOutputProbName{"prob1"};
  const size_t kInputC{3};

  size_t input_w_{0};
  size_t input_h_{0};
  size_t out_prob_size_;
  size_t out_loc_size_;

  int input_index_;
  int output_prob_;
  int output_loc_;

  void* buffers_[3];

  float p_threshold_{0.6f};
  float nms_threshold_{0.5f};  // TODO: check if we really need this shit here

  PBox::Sptr loc_{nullptr};
  PBox::Sptr score_{nullptr};
  PBox::Sptr rgb_{nullptr};

  std::vector<BBox::Sptr> bboxes_;
  std::vector<OrderScore::Sptr> bbox_scores_;

  void BuildCaffeEngine();
  void GenerateBBox(float scale);
};

}  // namespace omv

#endif  // OMV_PNET_H
