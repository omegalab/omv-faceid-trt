#ifndef OM_HELPER_H
#define OM_HELPER_H

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>

#include "logger.h"

#define CHECK(status)                             \
  do {                                            \
    auto ret = (status);                          \
    if (ret != 0) {                               \
      LOG(omv::ERROR) << "Cuda failure: " << ret; \
      abort();                                    \
    }                                             \
  } while (0)

constexpr long double operator"" _GiB(long double val) { return val * (1 << 30); }
constexpr long double operator"" _MiB(long double val) { return val * (1 << 20); }
constexpr long double operator"" _KiB(long double val) { return val * (1 << 10); }

constexpr long long unsigned int operator"" _GiB(long long unsigned int val) { return val * (1 << 30); }
constexpr long long unsigned int operator"" _MiB(long long unsigned int val) { return val * (1 << 20); }
constexpr long long unsigned int operator"" _KiB(long long unsigned int val) { return val * (1 << 10); }

namespace omv {

struct PBox {
  using Sptr = std::shared_ptr<PBox>;
  using Uptr = std::unique_ptr<PBox>;
  PBox() = default;
  PBox(size_t w, size_t h, size_t c);

  size_t width;
  size_t height;
  size_t channel;
  float* pdata;
};

struct BBox {
  using Sptr = std::shared_ptr<BBox>;
  using Uptr = std::unique_ptr<BBox>;
  bool exist;
  size_t x1;
  size_t y1;
  size_t x2;
  size_t y2;
  float score;
  float area;
  float ppoint[10];
  float regre_coord[4];
};

struct OrderScore {
  using Sptr = std::shared_ptr<OrderScore>;
  using Uptr = std::unique_ptr<OrderScore>;
  OrderScore() = default;
  OrderScore(float s, int o);
  float score;
  int ori_order;
};

struct ShowVersion : public std::runtime_error {
  ShowVersion(const std::string& msg);
};

struct FourCC {
  static int32_t ToInt(const std::string& v);
  static std::string FromInt(int32_t val);
};

struct InferDeleter {
  template <typename T>
  void operator()(T* obj) const {
    if (obj)
      obj->destroy();
  }
};

template <typename I>
using InferUptr = std::unique_ptr<I, InferDeleter>;

template <typename I>
using InferSptr = std::shared_ptr<I>;

template <typename I, typename... Args>
static InferUptr<I> MakeUniqueInfer(Args&&... args) {
  return InferUptr<I>(std::forward<Args>(args)...);
}

template <typename I, typename... Args>
static InferUptr<I>&& MakeUniqueInfer1(Args&&... args) {
  return std::move(InferUptr<I>(std::forward<Args>(args)...));
}

template <typename I, typename... Args>
static InferSptr<I> MakeSharedInfer(Args&&... args) {
  return InferSptr<I>(std::forward<Args>(args)..., InferDeleter());
}

class Helper {
 public:
  Helper() = default;
  enum class NMSModel { UNION = 0, MIN };

  template <typename It>
  static std::string PrintRange(const It& begin, const It& end) {
    std::ostringstream os;
    bool first{true};

    for (auto it = begin; it != end; ++it) {
      if (!first)
        os << ' ';
      os << *it;
      first = false;
    }

    return os.str();
  }

  template <typename T = size_t>
  static std::string HumanSize(T size) {
    static const char* kSizes[] = {"B", "KB", "MB", "GB"};
    size_t div = 0;
    size_t rem = 0;

    while (size >= 1024 && div < (sizeof kSizes / sizeof *kSizes)) {
      rem = (size % 1024);
      div++;
      size /= 1024;
    }

    float size_d = static_cast<float>(size) + static_cast<float>(rem) / 1024.0f;
    return std::to_string(static_cast<T>(std::ceil(size_d))) + " " + kSizes[div];
  }

  static int64_t GetTimeStamp();
  static bool FileExists(const std::string& path);

  static void SetThreadName(const std::string& name);
  static void SetThreadName(unsigned long, const std::string&);
  static std::vector<std::string> SplitString(const std::string& source, char delimiter);
  static void Mat2Array(const cv::Mat& image, PBox::Sptr pbox);
  static void NMS(std::vector<BBox::Sptr>&, std::vector<OrderScore::Sptr>&, const float,
                  NMSModel = NMSModel::UNION);
};

}  // namespace omv

#endif  // OM_HELPER_H
