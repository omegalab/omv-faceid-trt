#include "options.h"

#include "fmt/format.h"

#include "helper.h"
#include "logger.h"

namespace omv {

void Options::Init(int argc, char** argv) {
  argc_ = argc;
  argv_ = argv;

  opts_.add_option<Variant, std::string>("-d,--data", data_["data"], "Data directory");
  opts_.add_option<Variant, int>("--dtype", data_["dtype"], "Data Type (0-FP32, 1-FP16)");
  opts_.add_option<Variant, int>("--batch-size", data_["batch-size"], "Batch size (default=1)");
  opts_.add_option<Variant, int>("--min-face-size", data_["min-face-size"], "Minimal face size (default=60)");
  opts_.add_option<Variant, int>("--min-det-size", data_["min-det-size"],
                                 "Minimal detalization size (default=12)");
  opts_.add_option<Variant, int>("--max-scene-faces", data_["max-scene-faces"],
                                 "Minimal detalization size (default=10)");
  opts_.add_flag_callback(
      "--rebuild", [&] { data_["rebuild"] = true; }, "Rebuild engines");
  opts_.add_flag_callback(
      "--version", [&] { throw ShowVersion(opts_.get_description()); }, "Show version information");

  opts_.add_flag_function(
      "-v",
      [&](int count) {
        OmvSeverity log_level = OmvSeverity::DEBUG;

        if (count < 4)
          log_level = static_cast<OmvSeverity>(count + 1);

        Logger::Instance().SetSeverityLevel(log_level);
      },
      "Verbose level flag");
}

void Options::Exit(CLI::ParseError& e) { opts_.exit(e); }
void Options::Parse() { opts_.parse(argc_, argv_); }

Options::Options()
    : opts_{fmt::format("{} (version: {}; build: {})", PROJECT_NAME, PROJECT_VERSION, PROJECT_BUILD_DATE)} {}

}  // namespace omv
