#ifndef OMV_RNET_H
#define OMV_RNET_H

#include "engine.h"
#include "logger.h"

namespace omv {

class RNet : public IEngine {
 public:
  using Sptr = std::shared_ptr<RNet>;
  using Uptr = std::unique_ptr<RNet>;

  RNet();
  ~RNet() override;

  void Init(size_t rows, size_t cols) override;
  void Run(cv::Mat& image, float scale = 0) override;

 protected:
  void CudaMemAlloc() override;
  void CudaMemFree() override;

 private:
  const std::string kInputBlobName{"data"};
  const std::string kOutputLocName{"conv5-2"};
  const std::string kOutputProbName{"prob1"};
  const size_t kInputC{3};
  size_t input_w_{0};
  size_t input_h_{0};
  size_t out_prob_size_;
  size_t out_loc_size_;
  int input_index_;
  int output_prob_;
  int output_loc_;

  void* buffers_[3];
  float r_threshold_{0.7f};

  PBox::Sptr loc_{nullptr};
  PBox::Sptr score_{nullptr};
  PBox::Sptr rgb_{nullptr};
};

}  // namespace omv

#endif  // OMV_RNET_H
