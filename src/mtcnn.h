#ifndef OMV_MTCNN_H
#define OMV_MTCNN_H

#include "onet.h"
#include "pnet.h"
#include "rnet.h"

namespace omv {

class MTCNN {
 public:
  using Uptr = std::unique_ptr<MTCNN>;
  using Sptr = std::shared_ptr<MTCNN>;

  MTCNN() = default;
  void Init(size_t, size_t);

  void InitPNets(size_t, size_t);
  void InitRNet(size_t, size_t);
  void InitONet(size_t, size_t);
  void FindFaces(cv::Mat&, std::vector<BBox::Sptr>&);

 private:
  cv::Mat resized_image_;
  std::array<float, 3> nms_thresholds_;
  std::vector<float> scales_;
  std::vector<PNet::Sptr> pnets_;
  RNet::Sptr rnet_{nullptr};
  ONet::Sptr onet_{nullptr};

  std::vector<BBox::Sptr> first_bboxes_;
  std::vector<BBox::Sptr> second_bboxes_;
  std::vector<BBox::Sptr> third_bboxes_;
  std::vector<OrderScore::Sptr> first_order_scores_;
  std::vector<OrderScore::Sptr> second_order_scores_;
  std::vector<OrderScore::Sptr> third_order_scores_;
};

}  // namespace omv

#endif  // OMV_MTCNN_H
