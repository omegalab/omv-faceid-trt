#include <chrono>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>

#include "logger.h"
#include "omvfid.h"

int main(int argc, char* argv[]) {
  int exit_code = EXIT_SUCCESS;

  omv::Logger::Instance().Init();
  omv::OmvFID omvfid(argc, argv);

  try {
    omvfid.Init();
    omvfid.Start();

    //    std::thread tr([] {
    //      while (true) {
    //        LOGX << "SLEEEEEP";
    //        std::this_thread::sleep_for(std::chrono::seconds(1));
    //      }
    //    });

    //    tr.join();

  } catch (omv::ShowVersion& e) {
    omvfid.ShowVersionAndExit(e);
  } catch (CLI::ParseError& e) {
    omvfid.ShowHelpAndExit(e);
  } catch (std::exception& e) {
    exit_code = EXIT_FAILURE;
    LOG(omv::ERROR) << e.what();
  }

  return exit_code;
}
