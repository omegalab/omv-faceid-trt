#include "facenet.h"

#include <cassert>
#include <fstream>

#include "fmt/format.h"

#include "helper.h"
#include "logger.h"
#include "options.h"

namespace omv {

FaceNet::~FaceNet() {
  nvuffparser::shutdownProtobufLibrary();
  CudaMemFree();
}

void FaceNet::Init(size_t, size_t) {
  auto& opts = Options::Instance();
  const std::string& data_path = opts["data"];
  const bool rebuild_engine = opts["rebuild"];

  const std::string& model_path = fmt::format("{}/{}", data_path, "facenet.uff");
  const std::string& engine_path = fmt::format("{}/{}", data_path, "facenet.engine");

  SetModelPath(model_path);
  SetEnginePath(engine_path);

  if (Helper::FileExists(engine_path) && !rebuild_engine)
    LoadEngine();
  else
    BuildEngine<nvuffparser::IUffParser>("gnet");

  auto engine = GetEngine();

  if (nullptr == engine)
    throw std::runtime_error(fmt::format("null engine: {}", GetEnginePath()));

  input_index_ = engine->getBindingIndex("input");
  output_index_ = engine->getBindingIndex("embeddings");

  CudaMemAlloc();
}

void FaceNet::CudaMemAlloc() {
  auto batch_size = GetBatchSize();

  size_t size_of_single_input = 3 * 160 * 160 * sizeof(float);
  size_t size_of_single_output = 128 * sizeof(float);

  CHECK(cudaMalloc(&buffers_[input_index_], batch_size * size_of_single_input));
  CHECK(cudaMalloc(&buffers_[output_index_], batch_size * size_of_single_output));
  CreateCudaStream();
}

void FaceNet::CudaMemFree() {
  DestroyCudaStream();
  CHECK(cudaFree(buffers_[input_index_]));
  CHECK(cudaFree(buffers_[output_index_]));
}

}  // namespace omv
