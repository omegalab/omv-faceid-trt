#ifndef OMV_ONET_H
#define OMV_ONET_H

#include "engine.h"

namespace omv {

class ONet : public IEngine {
 public:
  using Sptr = std::shared_ptr<ONet>;
  using Uptr = std::unique_ptr<ONet>;

  ONet();
  ~ONet() override;

  void Init(size_t rows, size_t cols) override;
  void Run(cv::Mat& image, float scale = 0) override;

 protected:
  void CudaMemAlloc() override;
  void CudaMemFree() override;

 private:
  const std::string kInputBlobName{"data"};
  const std::string kOutputLocName{"conv6-2"};
  const std::string kOutputProbName{"prob1"};
  const std::string kOutputPointName{"conv6-3"};
  const size_t kInputC{3};
  size_t input_w_{0};
  size_t input_h_{0};
  size_t out_prob_size_;
  size_t out_loc_size_;
  size_t out_points_size_;
  int input_index_;
  int output_prob_;
  int output_loc_;
  int output_points_;

  void* buffers_[4];
  float o_threshold_{0.8f};

  PBox::Sptr loc_{nullptr};
  PBox::Sptr score_{nullptr};
  PBox::Sptr rgb_{nullptr};
  PBox::Sptr points_{nullptr};
};

}  // namespace omv

#endif  // OMV_ONET_H
