#include "omvfid.h"

#include <NvInfer.h>
#include <NvInferPlugin.h>
#include <NvUffParser.h>

#include <functional>

#include "l2norm_helper.h"
#include "logger.h"

namespace omv {

std::function<void()> SignalHandler::callback_ = nullptr;

OmvFID::OmvFID(int argc, char* argv[]) { Options::Instance().Init(argc, argv); }

void OmvFID::Init() {
  Options::Instance().Parse();

  if (!initLibNvInferPlugins(&Logger::Instance(), ""))
    throw std::runtime_error("failed to init NvInferPlugins!");

  signal_handler_.RegisterCallback([&]() { Stop(); });

  //  facenet_ = std::make_unique<FaceNet>();
  //  facenet_->Init(720, 1280);

  mtcnn_ = std::make_unique<MTCNN>();
  mtcnn_->Init(720, 1280);
}

void OmvFID::Start() {
  //  std::thread tr([&] {
  //    while (!stopped_) {
  //      LOG(INFO) << "threeeeeeeed";
  //      std::this_thread::sleep_for(std::chrono::seconds(1));
  //    }
  //  });
  //  tr.join();

  cv::Mat img = cv::imread("/home/rs/Omega/omv-faceid-trt/2020-03-08-093940.jpg");

  std::vector<BBox::Sptr> face_boxes;
  mtcnn_->FindFaces(img, face_boxes);
}

void OmvFID::Stop() {
  LOG(INFO) << "signalll";
  stopped_.store(true);
}

void OmvFID::ShowHelpAndExit(CLI::ParseError& e) { Options::Instance().Exit(e); }
void OmvFID::ShowVersionAndExit(omv::ShowVersion& e) { std::cout << e.what() << std::endl; }

void SignalHandler::RegisterCallback(std::function<void()>&& cb) {
  SignalHandler::callback_ = std::move(cb);
  signal(SIGINT, &SignalHandler::CatchSignal);
  signal(SIGTERM, &SignalHandler::CatchSignal);
}

void SignalHandler::CatchSignal(int s) {
  if (s == SIGINT || s == SIGTERM)
    callback_();
}

}  // namespace omv
