#include "rnet.h"

#include "logger.h"
#include "options.h"

namespace omv {

RNet::RNet() {
  loc_ = std::make_shared<PBox>(1, 1, 4);
  score_ = std::make_shared<PBox>(1, 1, 2);
  rgb_ = std::make_shared<PBox>();
}
RNet::~RNet() {
  nvcaffeparser1::shutdownProtobufLibrary();

  if (loc_->pdata)
    std::free(loc_->pdata);
  if (score_->pdata)
    std::free(score_->pdata);
  if (rgb_->pdata)
    std::free(rgb_->pdata);

  CudaMemFree();
}

void RNet::Init(size_t rows, size_t cols) {
  input_h_ = rows;
  input_w_ = cols;

  auto& opts = Options::Instance();
  const std::string& data_path = opts["data"];
  const bool rebuild_engine = opts["rebuild"];

  const std::string& proto_path = fmt::format("{}/{}", data_path, "det2_relu.prototxt");
  const std::string& model_path = fmt::format("{}/{}", data_path, "det2_relu.caffemodel");
  const std::string& engine_path = fmt::format("{}/{}", data_path, "det2_relu.engine");

  SetProtoPath(proto_path);
  SetModelPath(model_path);
  SetEnginePath(engine_path);
  SetInputBlobName(kInputBlobName);
  SetOutputLocName(kOutputLocName);
  SetOutputProbName(kOutputProbName);

  if (Helper::FileExists(engine_path) && !rebuild_engine)
    LoadEngine();
  else
    BuildEngine<nvcaffeparser1::ICaffeParser>("rnet");

  auto engine = GetEngine();

  if (nullptr == engine)
    throw std::runtime_error(fmt::format("null RNet engine: {}", GetEnginePath()));

  input_index_ = engine->getBindingIndex(kInputBlobName.c_str());
  output_prob_ = engine->getBindingIndex(kOutputProbName.c_str());
  output_loc_ = engine->getBindingIndex(kOutputLocName.c_str());

  out_prob_size_ = score_->width * score_->height * score_->channel;
  out_loc_size_ = loc_->width * loc_->height * loc_->channel;

  rgb_->pdata = static_cast<float*>(std::malloc(kInputC * input_h_ * input_w_ * sizeof(float)));
  score_->pdata = static_cast<float*>(std::malloc(2 * sizeof(float)));
  loc_->pdata = static_cast<float*>(std::malloc(4 * sizeof(float)));

  CudaMemAlloc();
}

void RNet::Run(cv::Mat& image, float scale) {}

void RNet::CudaMemAlloc() {
  auto batch_size = GetBatchSize();

  CHECK(cudaMalloc(&buffers_[input_index_], batch_size * kInputC * input_h_ * input_w_ * sizeof(float)));
  CHECK(cudaMalloc(&buffers_[output_prob_], batch_size * out_prob_size_ * sizeof(float)));
  CHECK(cudaMalloc(&buffers_[output_loc_], batch_size * out_loc_size_ * sizeof(float)));
  CreateCudaStream();
}

void RNet::CudaMemFree() {
  DestroyCudaStream();
  CHECK(cudaFree(buffers_[input_index_]));
  CHECK(cudaFree(buffers_[output_prob_]));
  CHECK(cudaFree(buffers_[output_loc_]));
}

}  // namespace omv
