#include "helper.h"

#include <pthread.h>

#include <algorithm>
#include <chrono>

#include <opencv2/opencv.hpp>

#include "logger.h"

namespace omv {

ShowVersion::ShowVersion(const std::string& msg) : std::runtime_error{msg} {}

void Helper::SetThreadName(const std::string& name) { pthread_setname_np(pthread_self(), name.c_str()); }
void Helper::SetThreadName(unsigned long thread_id, const std::string& name) {
  pthread_setname_np(thread_id, name.c_str());
}

int64_t Helper::GetTimeStamp() {
  using namespace std::chrono;
  return static_cast<int64_t>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
}

bool Helper::FileExists(const std::string& path) {
  std::ifstream f(path.c_str());
  return f.good();
}

std::vector<std::string> Helper::SplitString(const std::string& source, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream token_stream(source);

  while (std::getline(token_stream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}

void Helper::Mat2Array(const cv::Mat& image, PBox::Sptr pbox) {
  if ((image.data == nullptr) || (image.type() != CV_8UC3)) {
    LOG(ERROR) << "image's type is wrong!!Please set CV_8UC3";
    return;
  }

  if (pbox->pdata == nullptr)
    return;

  float* p = pbox->pdata;

  for (int rowI = 0; rowI < image.rows; ++rowI) {
    for (int colK = 0; colK < image.cols; ++colK) {
      *p = (image.at<cv::Vec3b>(rowI, colK)[2] - 127.5f) * 0.007812f;
      *(p + image.rows * image.cols) = (image.at<cv::Vec3b>(rowI, colK)[1] - 127.5f) * 0.0078125f;
      *(p + 2 * image.rows * image.cols) = (image.at<cv::Vec3b>(rowI, colK)[0] - 127.5f) * 0.0078125f;
      ++p;
    }
  }

  return;
}

void Helper::NMS(std::vector<BBox::Sptr>& bboxes, std::vector<OrderScore::Sptr>& scores, const float threshold,
                 Helper::NMSModel nms_model) {
  if (bboxes.empty())
    return;

  std::vector<size_t> heros;
  std::sort(scores.begin(), scores.end(),
            [](OrderScore::Sptr lhs, OrderScore::Sptr rhs) { return (lhs->score < rhs->score); });

  int order{0};
  float IOU{0};
  float max_x{0};
  float max_y{0};
  float min_x{0};
  float min_y{0};

  while (scores.size() > 0) {
    order = scores.back()->ori_order;
    scores.pop_back();

    if (order < 0)
      continue;

    size_t uorder = static_cast<size_t>(order);

    heros.push_back(uorder);
    bboxes.at(uorder)->exist = false;  // delete it

    for (size_t num = 0; num < bboxes.size(); ++num) {
      if (bboxes.at(num)->exist) {
        // the iou
        max_x = (bboxes.at(num)->x1 > bboxes.at(uorder)->x1) ? bboxes.at(num)->x1 : bboxes.at(uorder)->x1;
        max_y = (bboxes.at(num)->y1 > bboxes.at(uorder)->y1) ? bboxes.at(num)->y1 : bboxes.at(uorder)->y1;
        min_x = (bboxes.at(num)->x2 < bboxes.at(uorder)->x2) ? bboxes.at(num)->x2 : bboxes.at(uorder)->x2;
        min_y = (bboxes.at(num)->y2 < bboxes.at(uorder)->y2) ? bboxes.at(num)->y2 : bboxes.at(uorder)->y2;

        // maxX1 and maxY1 reuse
        max_x = ((min_x - max_x + 1) > 0) ? (min_x - max_x + 1) : 0;
        max_y = ((min_y - max_y + 1) > 0) ? (min_y - max_y + 1) : 0;

        // IOU reuse for the area of two bbox
        IOU = max_x * max_y;

        if (nms_model == NMSModel::UNION)
          IOU = IOU / ((bboxes.at(num)->area < bboxes.at(uorder)->area) ? bboxes.at(num)->area
                                                                        : bboxes.at(uorder)->area);
        else if (nms_model == NMSModel::MIN)
          IOU = IOU / (bboxes.at(num)->area + bboxes.at(uorder)->area - IOU);

        if (IOU > threshold) {
          bboxes.at(num)->exist = false;
          for (auto it = scores.begin(); it != scores.end(); ++it) {
            if ((*it)->ori_order == static_cast<int>(num)) {
              (*it)->ori_order = -1;
              break;
            }
          }
        }
      }
    }
  }
  for (size_t i = 0; i < heros.size(); ++i)
    bboxes.at(heros.at(i))->exist = true;
}

int32_t FourCC::ToInt(const std::string& v) { return (((((v[3] << 8) | v[2]) << 8) | v[1]) << 8) | v[0]; }

std::string FourCC::FromInt(int32_t val) {
  std::string st(4, 0);
  st[0] = static_cast<char>(val & 0xff);
  st[1] = static_cast<char>((val >> 8) & 0xff);
  st[2] = static_cast<char>((val >> 16) & 0xff);
  st[3] = static_cast<char>((val >> 24) & 0xff);
  return st;
}

PBox::PBox(size_t w, size_t h, size_t c) : width{w}, height{h}, channel{c} {}
OrderScore::OrderScore(float s, int o) : score{s}, ori_order{o} {}

}  // namespace omv
